from django.db import models
from django.utils import timezone

class Message(models.Model):
    title = models.CharField('宛先', max_length=500)
    text = models.TextField('本文')
    date = models.DateTimeField('日付', default = timezone.now)

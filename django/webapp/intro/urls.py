from django.urls import path
from .import views

app_name = 'intro'

urlpatterns = [
    path('', views.index, name = 'index'), #/intro
    path('masaru/', views.masaru, name = 'masaru'), #/masaru
    path('masaki/', views.masaki, name = 'masaki'), #/masaki
    path('yuka/', views.yuka, name = 'yuka'), #/yuka
    path('makiyama/', views.makiyama, name = 'makiyama'), #/makiyama
    path('add/', views.add, name = 'add'), 
    path('_list', views._list, name = '_list'), 
    path('update/<int:pk>/', views.update, name ='update'),
    path('delete/<int:pk>/', views.delete, name = 'delete'),
    path('detail/<int:pk>/', views.detail, name = 'detail'),
]
from django.shortcuts import render, redirect, get_object_or_404
from .forms import MessageCreateForm
from .models import Message

# Create your views here.
def index(request):
    return render(request, 'intro/home.html')

def masaru(request):
    return render(request, 'intro/masaru.html')

def masaki(request):
    return render(request, 'intro/masaki.html')

def yuka(request):
    return render(request, 'intro/yuka.html')

def makiyama(request):
    return render(request, 'intro/makiyama.html')

def _list(request):
    context ={
        'message_list':Message.objects.all()
    }
    return render(request, 'intro/message_list.html', context)
    
def add(request):

    form = MessageCreateForm(request.POST or None)

    if request.method =='POST' and form.is_valid():
        form.save()
        return redirect('intro:_list')
    context = {
        'form':form
    }
    return render(request, 'intro/message_form.html', context)

def update(request, pk):

    message = get_object_or_404(Message, pk=pk)

    form = MessageCreateForm(request.POST or None, instance = message)

    if request.method =='POST' and form.is_valid():
        form.save()
        return redirect('intro:_list')
    context = {
        'form':form
    }
    return render(request, 'intro/message_form.html', context)

def delete(request, pk):
    message = get_object_or_404(Message, pk=pk)

    if request.method =='POST':
        message.delete()
        return redirect('intro:_list')

    context = {
        'message':message,
    }
    return render(request, 'intro/message_confirm_delete.html', context)

def detail(request, pk):
    message = get_object_or_404(Message, pk=pk)

    context ={
        'message':message
    }

    return render(request, 'intro/message_detail.html', context)


